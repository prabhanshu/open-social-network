import express from 'express'
import http from 'http'
import bodyParser from 'body-parser'
import socketIo from 'socket.io'
import path from 'path'
// import mongoose from 'mongoose'
// import morgan from 'morgan'

import { config } from './config/config'
import samplePosts from './samplePosts.json'
import sampleComments from './sampleComments.json'

const app = express()
const apiRouter = express.Router()
const server = http.createServer(app)
const io = socketIo(server)

const port = (process.env.PORT || config.server.port)

let postData = [...samplePosts]
let commentData = [...sampleComments]
/*
mongoose.Promise = global.Promise
mongoose.connect(config.dbUri)
  .then(() => {
    console.log('Connected to database')
  })
  .catch((err) => {
    console.log(`Error connecting to database: ${err}`)
  })

app.use(morgan('dev'))
*/

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

if (process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
  app.use(express.static(path.resolve(__dirname, '../build')))
}

app.use((req, res, next) => {
  req.requestQuery = {
    body: req.body,
    query: req.query,
    merged: Object.assign({}, req.body, req.query)
  }

  console.info()
  console.info('====Middleware====')
  console.info('Req Query: ', req.requestQuery)
  console.info('==================')
  console.info()
  next()
})

// TODO: use controllers and models to save posts and comments in mongo db
let db = (data) => {
  console.log(data)
}

apiRouter.get('/', (req, res) => {
  res.json({
    success: true,
    extras: {
      message: 'Hello World'
    }
  })
})

io.on('connection', socket => {
  console.log('connection socket')
  socket.emit('initialPosts', postData)
  socket.emit('comments', commentData)

  socket.on('comments', comment => {
    commentData = [...commentData, comment]
    db(comment)
    socket.emit('more-comments', [comment])
  })

  socket.on('post', post => {
    postData = [...postData, post]
    db(post)
    socket.emit('more-posts', [post])
  })
})

app.use('/api', apiRouter)
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../build', 'index.html'))
})

server.listen(port)
console.log('Serving at ', port)
