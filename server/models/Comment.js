import mongoose from 'mongoose'
import uuid from 'node-uuid'

let Schema = mongoose.Schema

var CommentSchema = new Schema({
  _id: {
    type: String,
    default: uuid.v4
  },
  postId: {
    type: String,
    ref: 'Post',
    required: true
  },
  userId: {
    type: String,
    ref: 'User',
    required: true
  },
  text: {
    type: String,
    required: true
  },
  createdTimestamp: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Comment', CommentSchema)
