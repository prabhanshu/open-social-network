import mongoose from 'mongoose'
import uuid from 'node-uuid'

let Schema = mongoose.Schema

var PostSchema = new Schema({
  _id: {
    type: String,
    default: uuid.v4
  },
  userId: {
    type: String,
    ref: 'User',
    required: true
  },
  text: {
    type: String,
    required: true
  },
  createdTimestamp: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Post', PostSchema)
