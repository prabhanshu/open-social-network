import mongoose from 'mongoose'
import uuid from 'node-uuid'

let Schema = mongoose.Schema

var UserSchema = new Schema({
  _id: {
    type: String,
    default: uuid.v4
  },
  userId: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  createdTimestamp: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('User', UserSchema)
