import idb from 'idb'
import uuidv4 from 'uuid/v4'

class IndexDb {
  constructor (config) {
    this.state = {
      config: config,
      version: config.version
    }
    this.db = idb.open(
      this.state.config.dbName,
      this.state.config.version,
      (upgradeDb) => {
        switch (upgradeDb.oldVersion) {
          case 0:
          case 1:
            let version = 1
            let stores = this.state.config.stores[version]
            for (let i in stores) {
              let s = stores[i]
              let key = s.key
              let indexes = s.indexes

              let store = upgradeDb.createObjectStore(s.id, {
                keyPath: key
              })
              for (let k in indexes) {
                store.createIndex(k, indexes[k])
              }
            }
            break
          default:
            break
        }
      }
    )
  }

  fetchUserIdFromDb = (name,callback) => {
    this.getUser().then((user) => {
      if (user.length > 0) {
        callback(user[0])
      } else {
        this.saveUser({ id: uuidv4(), name: name }).then((u) => {
          this.getUser().then((user) => {
            if (user) {
              callback(user[0])
            }
          })
        })
      }
    })
  }

  getUser = () => this.db.then((db) => {
    if (!db) return

    let storesData = this.state.config.stores[this.state.version]
    let storeId = storesData.user.id
    let tx = db.transaction(storeId, 'readonly')
    let store = tx.objectStore(storeId)
    return store.getAll()
  })

  saveUser = (userData) => this.db.then((db) => {
    if (!db) return

    let storesData = this.state.config.stores[this.state.version]
    let storeId = storesData.user.id
    let tx = db.transaction(storeId, 'readwrite')
    let store = tx.objectStore(storeId)
    if(userData) {
      store.clear()
    }
    store.put(userData)
    return tx.complete
  })

  addPosts = (posts) => {
    this.db.then((db) => {
      if (!db) return

      let storesData = this.state.config.stores[this.state.version]
      let storeId = storesData.posts.id
      let tx = db.transaction(storeId, 'readwrite')
      let store = tx.objectStore(storeId)
      for (let k in posts) {
        store.delete(k);
        store.put(posts[k])
      }
      return tx.complete
    })
  }

  getAllPosts = (userId) => this.db.then((db) => {
    if (!db) return

    let storesData = this.state.config.stores[this.state.version]
    let storeId = storesData.posts.id
    let tx = db.transaction(storeId, 'readonly')
    let store = tx.objectStore(storeId)
    if (userId) {
      let index = store.index('by-user')
      return index.getAll(userId)
    } else {
      let index = store.index('by-date')
      return index.getAll()
    }
  })

  addComments = (comments) => {
    this.db.then((db) => {
      if (!db) return

      let storesData = this.state.config.stores[this.state.version]
      let storeId = storesData.comments.id
      let tx = db.transaction(storeId, 'readwrite')
      let store = tx.objectStore(storeId)
      for (let k in comments) {
        store.delete(k);
        store.put(comments[k])
      }
      return tx.complete
    })
  }
  
  getCommentsOfPost = (postId) => this.db.then((db) => {
    if (!db) return

    let storesData = this.state.config.stores[this.state.version]
    let storeId = storesData.comments.id
    let tx = db.transaction(storeId, 'readonly')
    let index = tx.objectStore(storeId).index('by-post')
    return index.getAll(postId)
  })

  getPostsOfUser = (userId) => this.db.then((db) => {
    if (!db) return

    let storesData = this.state.config.stores[this.state.version]
    let storeId = storesData.posts.id
    let tx = db.transaction(storeId, 'readonly')
    let index = tx.objectStore(storeId).index('by-user')
    return index.getAll(userId)
  })

}

export default IndexDb
