import React, { Component } from 'react'
import { Avatar, Col } from 'antd'
import moment from 'moment'
import TimeAgo from 'react-timeago'

import Comments from './comments'

class Post extends Component {
  constructor (props) {
    super(props)
    this.state = {
      config: this.props.config,
      post: this.props.post,
      comments: []
    }
  }

  render () {
    let timeString = <TimeAgo date={moment(this.state.post.timestamp).format('YYYY-MM-DD HH:mm:ss')} minPeriod={10} />
    let userNameInitials = this.state.post.userName.match(/\b\w/g) || ['U']
    userNameInitials = ((userNameInitials.shift() || '') + (userNameInitials.pop() || '')).toUpperCase()
    let avatarColor = this.state.post.userId.substring(0, 6)

    return (
      <Col span={24} className='post'>
        <div className='head'>
          <div style={{ marginRight: '10px' }}>
            <Avatar style={{ backgroundColor: `#${avatarColor}` }}>
              {userNameInitials}
            </Avatar>
          </div>
          <div className='post-details'>
            <div>
              <a href={`${this.state.config.slugs.user}/${this.state.post.userId}`}>
                {this.state.post.userName}
              </a>
            </div>
            <div className='timestamp'>
              {timeString}
            </div>
          </div>
        </div>
        <div className='content'>
          {this.state.post.text}
        </div>
        <Comments config={{...this.state.config, postId: this.state.post.id}} />
      </Col>
    )
  }
}

export default Post
