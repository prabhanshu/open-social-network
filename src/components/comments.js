import React, { Component } from 'react'
import { Input } from 'antd'
import uuidv4 from 'uuid/v4'
import moment from 'moment'
import TimeAgo from 'react-timeago'

class Comment extends Component {
  constructor (props) {
    super(props)
    this.state = {
      config: this.props.config,
      comments: []
    }
    this.updateComments = this.updateComments.bind(this)
    this.handleCommentSubmit = this.handleCommentSubmit.bind(this)
  }

  updateComments (comments) {
    comments.sort((a, b) => {
      let keyA = new Date(a.timestamp)
      let keyB = new Date(b.timestamp)
      if (keyA < keyB) return -1
      return 1
    })
    this.setState({ comments: comments })
  }

  componentDidMount () {
    this.state.config.db.getCommentsOfPost(this.state.config.postId).then((comments) => {
      this.updateComments(comments)
    })

    this.state.config.socket.on('more-comments', comments => {
      this.state.config.db.getCommentsOfPost(this.state.config.postId).then((comments) => {
        this.updateComments(comments)
      })
    })
  }

  handleCommentSubmit (event) {
    const commentText = event.target.value
    if (event.keyCode === 13 && commentText) {
      this.state.config.db.fetchUserIdFromDb(this.state.config.user.name, (user) => {
        let comment = {
          id: uuidv4(),
          postId: this.state.config.postId,
          text: commentText,
          userId: user.id,
          userName: user.name,
          timestamp: new Date().getTime()
        }
        this.state.config.socket.emit('comments', comment)
      })
      event.target.value = ''
    }
  }

  render () {
    let comments = []
    for (let i in this.state.comments) {
      let c = this.state.comments[i]
      comments.push(<div className='comment' id={c.id} key={c.id}>
        <div style={{ marginRight: '10px' }}>
          <a href={`${this.state.config.slugs.user}/${c.userId}`}>
            {c.userName}
          </a>
        </div>
        <div className='text'>{ c.text }</div>
        <div className='timestamp'>{<TimeAgo date={moment(c.timestamp).format('YYYY-MM-DD HH:mm:ss')} minPeriod={5} />}</div>
      </div>)
    }

    return (
      <div className='comments'>
        {comments}
        <div className='commentInput'>
          <Input style={{ fontSize: '100%' }} placeholder='comment' onKeyUp={this.handleCommentSubmit} />
        </div>
      </div>
    )
  }
}

export default Comment
