import React, { Component } from 'react'
import { Row, Col, Input, Button,Icon } from 'antd'
import io from 'socket.io-client'
import uuidv4 from 'uuid/v4'

import Post from '../components/post'

const { TextArea } = Input

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      config: this.props.config,
      posts: {},
      draftPost: '',
      userPage: this.props.match.params.uId ? true : false,
      userPageId: this.props.match.params.uId ? this.props.match.params.uId : ''
    }
    this.db = this.state.config.db
  }

  getPosts () {
    if (!(this.state.userPage && this.state.userPageId)) {
      this.db.getAllPosts().then((posts) => {
        this.setState({ posts: posts.reverse() })
      })
    } else {
      this.db.getAllPosts(this.state.userPageId).then((posts) => {
        this.setState({ posts: posts.reverse() })
      })
    }
  }

  updatePosts = (p) => {
    this.db.addPosts(p)
    this.getPosts()
  }

  updateComments = (comments) => {
    this.db.addComments(comments)
    this.getPosts()
  }

  updateUserObjectStore = (n) => {
    this.db.fetchUserIdFromDb(this.state.config.user.name, (user) => {
      if (n && user.name !== n)  {
        this.db.saveUser({id: user.id, name: n }).then((u) => {
          this.setState({config: {
            ...this.state.config,
            user: {
              id: user.id,
              name: n
            }
          }})
        })
      } else {
        this.setState({config: {
          ...this.state.config,
          user: {
            id: user.id,
            name: user.name
          }
        }})
      }
    })
  }

  componentDidMount () {
    this.updateUserObjectStore()
    this.socket = io(this.state.config.socket.path)
    if (this.state.userPage && this.state.userPageId) {
      this.db.getAllPosts(this.state.userPageId).then((posts) => {
        this.setState({ posts: posts.reverse() })
      })
    } else {
      this.db.getAllPosts().then((posts) => {
        this.setState({ posts: posts.reverse() })
      })
    }

    this.socket.on('more-posts', p => {
      this.updatePosts(p)
    })

    this.socket.on('initialPosts', p => {
      this.updatePosts(p)
    })

    this.socket.on('more-comments', comments => {
      this.updateComments(comments)
    })

    this.socket.on('comments', comments => {
      this.setState({ posts: {}})
      this.updateComments(comments)
    })
  }

  handleNameSubmit = event => {
    const name = event.target.value
    if (event.keyCode === 13 && name) {
      this.updateUserObjectStore(name)
    }
  }

  handlePostChange = event => {
    const text = event.target.value
    this.setState({draftPost: text})
  }

  handlePost = event => {
    if (this.state.draftPost) {
      this.db.fetchUserIdFromDb(this.state.config.user.name, (user) => {
        let post = {
          id: uuidv4(),
          text: this.state.draftPost,
          userId: user.id,
          userName: user.name,
          timestamp: new Date().getTime()
        }
        this.socket.emit('post', post)
        this.setState({draftPost: ''})
      })
    }
  }

  render () {
    let posts = []
    // TODO: separate this into another component
    let topElement = <div>
              <div style={{ background: '#ffffff', padding: '10px', marginBottom: '20px' }}>
                <div style={{ marginBottom: '5px' }}>
                  Name: {this.state.config.user.name}
                </div>
                <div style={{ fontSize: '80%', marginBottom: '5px' }}>
                  ID: {this.state.config.user.id}
                </div>
                <div style={{ marginBottom: '20px'}}>
                  <Input placeholder="Enter your name to register" onKeyUp={this.handleNameSubmit} />
                </div>
                <div>
                  <TextArea ref={(input) => { this.postInput = input }} value={this.state.draftPost} style={{ marginBottom: '10px' }} rows={4} placeholder="Post something..." onChange={this.handlePostChange}/>
                  <Button onClick={this.handlePost} type="primary">Post</Button>
                </div>
              </div>
            </div>

    if (this.state.userPage && this.state.userPageId) {
      topElement = <div style={{ background: '#ffffff', padding: '10px', marginBottom: '10px' }}>
                <div style={{ marginBottom: '5px' }}>
                  User ID: {this.state.userPageId}
                </div>
              </div>
    }
    for (let p in this.state.posts) {
      let id = this.state.posts[p].id
      posts.push(<Row key={id}>
        <Post id={id} post={this.state.posts[p]} config={{...this.state.config, socket: this.socket}}/>
      </Row>)
    }
    if (posts.length === 0) {
      posts.push(<div key='loader' style={{
        fontSize: '200%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '300px'
      }}><Icon type="loading" /></div>)
    }

    // TODO: Infinite scrolling
    return (
      <div style={{ padding: '20px' }}>
        <Row type="flex" justify="center">
          <Col
            span={12}
            xs={24}
            sm={24}
            md={12}
            lg={12}
            xl={12}
            xxl={12}
          >
            {topElement}
            {posts}
          </Col>
        </Row>
      </div>
    )
  }
}

export default Home;
