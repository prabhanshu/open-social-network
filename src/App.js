import React, { Component } from 'react'
import { Layout } from 'antd'
import uuidv4 from 'uuid/v4'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import config from './config'
import HomePage from './pages/Home'
import IndexDb from './controllers/IndexDb.js'

import './css/dist/style.min.css'

const { Header, Content } = Layout

class App extends Component {
  constructor (props) {
    super(props)
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
      config.socket.path = config.socket.devPath
    }
    let userId = uuidv4()
    let userName = 'Darth Vader'
    this.state = {
      config: {
        ...config,
        db: new IndexDb(config.indexDb),
        user: {
          id: userId,
          name: userName
        }
      }
    }
  }

  render () {
    return (<div>
      <Layout>
        <Header style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          textAlign: 'center',
          padding: '10px',
          background: '#0a4a58',
          color: '#ffffff'
        }}>
          <div style={{ fontSize: '180%', fontWeight: '500', margin: '0px' }}>
            <a href='/' style={{ color: '#ffffff' }}>{ this.state.config.appName }</a>
          </div>
        </Header>
        <Content style={{ background: '#e9ebee' }}>
          <Router>
            <Switch>
              <Route
                exact path='/'
                render={(props) => <HomePage config={this.state.config} {...props} />}
              />
              <Route
                exact path={`${this.state.config.slugs.user}/:uId`}
                render={(props) => <HomePage config={this.state.config} {...props} />}
              />
            </Switch>
          </Router>
        </Content>
      </Layout>
    </div>)
  }
}

export default App
