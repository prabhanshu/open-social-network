const gulp = require('gulp')
const concat = require('gulp-concat')
const pump = require('pump')
const minify = require('gulp-minify-css')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const merge = require('merge-stream')

const paths = {
  styles: {
    src_css: './src/css/src/*.css',
    src_scss: './src/css/src/*.scss',
    dest: './src/css/dist/'
  }
}

gulp.task('style', function (cb) {
  let scssStream = gulp.src([paths.styles.src_scss])
    .pipe(sass())
    .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
    .pipe(concat('scss-files.scss'))

  let cssStream = gulp.src([paths.styles.src_css])
    .pipe(concat('css-files.css'))

  pump([
    merge(scssStream, cssStream),
    concat('style.min.css'),
    minify(),
    autoprefixer({browsers: ['last 2 versions'], cascade: false}),
    gulp.dest(paths.styles.dest)
  ], cb)
})

gulp.task('build', [
  'style'
])

gulp.task('default', [
  'style'
])
